# AWS LAMBDA NODE

  - Login
  - Token Generation
  - User Authorization
  - User Access control
  - Ressouce Access control

# Technologies

*   AWS Lambda 
* [TypeScript] - Object oriented Javascript
* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework
* [Gulp] - the streaming build system
* [Visual Source Code] - Best Typescript Editor


### Installation

Import from gitlab.

```sh
$ git clone https://gitlab.com/ganeshd.supra/aws-lambda-node.git
$ cd aws-lambda-node
```

Install the dependencies and devDependencies

```sh
$ npm install typescript -g
$ npm install
```

Compile and start the app

```sh
$ npm run build
$ npm start
```

   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>
   [TypeScript]: <https://www.typescriptlang.org/>
   [MongoDb]: <https://www.mongodb.com/>
   [Visual Source Code]: <https://code.visualstudio.com/>